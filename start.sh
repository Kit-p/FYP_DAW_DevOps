#!/bin/sh

# Resolve invocation username
USERNAME="${SUDO_USER:-$USER}"

# Resolve script absolute path
SCRIPTPATH=$(realpath "$0")
SCRIPTDIR=$(dirname "$SCRIPTPATH")

# Source shFlags.
. "$SCRIPTDIR"/setup/_shflags

# Configure shFlags.
DEFINE_boolean 'clean' true 'Stop and remove previous running services and their containers' 'c'
DEFINE_boolean 'purge' false 'Stop and remove previous running services, their containers and their volumes, implies --clean' 'p'
# FLAGS_HELP="USAGE: $0 [flags]"

# void function to exit this script with an optional error message
die() {
    [ $# -gt 0 ] && echo "Error: $@"
    flags_help
    exit 1
}

# bool function to test if the user is root or not
is_user_root() { [ "${EUID:-$(id -u)}" -eq 0 ]; }

# int function to execute command as the invocation user (i.e. without sudo)
unsudo() {
    sudo -H -u "$USERNAME" $@
    return $?
}

# Parse the command-line.
FLAGS "$@" || exit 1
eval set -- "${FLAGS_ARGV}"

# Check for root privileges
is_user_root || die "This script requires root privileges. You may want to run with 'sudo $0'."

# 'purge' flag implies 'clean' flag
if [ ${FLAGS_purge} -eq ${FLAGS_TRUE} ]; then
    FLAGS_clean=${FLAGS_TRUE}
fi

LOCALETCDIR="/usr/local/etc/daw"
rm -rf "$LOCALETCDIR"
mkdir -p "$LOCALETCDIR"
chmod 0700 "$LOCALETCDIR"

# Copy the Firebase credential
FIREBASECREDENTIALSSRCPATH="/home/$USERNAME/FYP2122_GCH2/credentials/firebase-service-account.json"
FIREBASECREDENTIALSDESTDIR="$LOCALETCDIR/credentials"
[ -f "$FIREBASECREDENTIALSSRCPATH" ] || die "Missing firebase credential at '$FIREBASECREDENTIALSSRCPATH'"
mkdir -p "$FIREBASECREDENTIALSDESTDIR"
cp "$FIREBASECREDENTIALSSRCPATH" "$FIREBASECREDENTIALSDESTDIR"
chmod -R 0400 "$FIREBASECREDENTIALSDESTDIR"
chmod 0700 "$FIREBASECREDENTIALSDESTDIR"

# Create the config files if not exist
LOCALCONFIGDIR="$LOCALETCDIR/mqtt"
mkdir -p "$LOCALCONFIGDIR"
cp -R "$SCRIPTDIR"/setup/emqx/* "$LOCALCONFIGDIR"
chmod -R 0400 "$LOCALCONFIGDIR"
touch "$LOCALCONFIGDIR/vm.args"
chmod 0666 "$LOCALCONFIGDIR/vm.args"
chmod 0700 "$LOCALCONFIGDIR"

LOCALCONFIGDIR="$LOCALETCDIR/envoy"
CONFIGFILEPATH="$LOCALCONFIGDIR/envoy.yaml"
mkdir -p "$LOCALCONFIGDIR"
cp "$SCRIPTDIR/setup/envoy/envoy.yaml" "$CONFIGFILEPATH"
chmod -R 0400 "$LOCALCONFIGDIR"
chmod 0700 "$LOCALCONFIGDIR"

LOCALCONFIGDIR="$LOCALETCDIR/apiserver"
CONFIGFILEPATH="$LOCALCONFIGDIR/config.json"
mkdir -p "$LOCALCONFIGDIR"
cp "/home/$USERNAME/FYP2122_GCH2/credentials/apiserver-config.json" "$CONFIGFILEPATH"
chmod -R 0400 "$LOCALCONFIGDIR"
chmod 0700 "$LOCALCONFIGDIR"

LOCALCONFIGDIR="$LOCALETCDIR/errorscanner"
CONFIGFILEPATH="$LOCALCONFIGDIR/config.json"
mkdir -p "$LOCALCONFIGDIR"
cp "/home/$USERNAME/FYP2122_GCH2/credentials/errorscanner-config.json" "$CONFIGFILEPATH"
chmod -R 0400 "$LOCALCONFIGDIR"
chmod 0700 "$LOCALCONFIGDIR"

# Copy the TLS Certs folders
CERTSSRCDIRPARENT="/home/$USERNAME/FYP2122_GCH2/credentials/tls-certs"
CERTSDIRS="db mqtt web"
for item in ${CERTSDIRS}; do
    CERTSDESTDIRPARENT="$LOCALETCDIR/$item"
    CERTSSRCDIR="$CERTSSRCDIRPARENT/$item"
    [ -d "$CERTSSRCDIR" ] || die "Missing certs folder at '$CERTSSRCDIR'."
    CERTSDESTDIR="$CERTSDESTDIRPARENT/certs"
    mkdir -p "$CERTSDESTDIRPARENT"
    cp -R "$CERTSSRCDIR" "$CERTSDESTDIR"
    mkdir "$CERTSDESTDIR/client"
    [ -f "$CERTSDESTDIR/ca.crt" ] && cp "$CERTSDESTDIR/ca.crt" "$CERTSDESTDIR/client/"
    mv -f "$CERTSDESTDIR"/client.* "$CERTSDESTDIR/client/" 2>/dev/null
    chmod -R 0500 "$CERTSDESTDIR"
    chmod 0700 "$CERTSDESTDIR/client"
    chmod 0700 "$CERTSDESTDIR"
done

# Make sure owner is correct
chown -f -R "$USERNAME:$USERNAME" "$LOCALETCDIR"

# Run the docker-compose.yml
UID=$(unsudo id -u)
GID=$(unsudo id -g)
if [ ${FLAGS_purge} -eq ${FLAGS_TRUE} ]; then
    unsudo UID=$UID GID=$GID docker-compose -f "$SCRIPTDIR/docker-compose.yml" down --remove-orphans --volumes
elif [ ${FLAGS_clean} -eq ${FLAGS_TRUE} ]; then
    unsudo UID=$UID GID=$GID docker-compose -f "$SCRIPTDIR/docker-compose.yml" down --remove-orphans
fi
unsudo UID=$UID GID=$GID docker-compose -f "$SCRIPTDIR/docker-compose.yml" up --detach --build --always-recreate-deps --remove-orphans

exit 0

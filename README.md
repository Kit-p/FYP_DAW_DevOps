# Dementia Anti-Wandering (FYP 2021-2022)
## Members
- PANG, Kit
- TAM, Tsz Chung
- WU, Tsz Yeung

---

## Description
This repository contains deployment related configurations and scripts in the GCH2 Final Year Project.  

## Environment
- Ubuntu Server 20.04 LTS
- Docker
- Docker Compose

## Folder Structure
| File / Folder                 | Description                                                       |
| ---:                          | :---                                                              |
| `start.sh`                    | Script for one-command deployment / redeployment (recommended)    |
| `docker-compose.yml`          | Defines the Docker services and their dependencies                |
| `setup/*`                     | Configuration files for each services                             |
| `setup/cockroachdb/init.sql`  | CockroachDB initialization SQL script, table structure reference  |

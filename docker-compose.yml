# version is only informative, this config follows The Composer Specification, which requires Docker Engine v19.03.0+.
version: '3.9'

# Services
services:

  # Envoy Reverse Proxy
  reverseproxy:
    container_name: daw_reverseproxy
    image: envoyproxy/envoy-alpine:v1.21.1
    pull_policy: missing
    restart: always
    hostname: daw_reverseproxy
    user: "${UID}:${GID}"
    environment:
      - TZ=Asia/Hong_Kong
    tty: false
    expose:
      - 8001
      - 10000
    ports:
      - target: 10000
        published: 80
        host_ip: 0.0.0.0
        mode: host
      - target: 10000
        published: 443
        host_ip: 0.0.0.0
        mode: host
    volumes:
      - type: bind
        source: /usr/local/etc/daw/envoy/envoy.yaml
        target: /etc/envoy/envoy.yaml
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/web/certs
        target: /certs/web
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/certs
        target: /certs/mqtt
        read_only: false
        bind:
          create_host_path: false

  # CockroachDB Database
  database:
    container_name: daw_database
    image: cockroachdb/cockroach:v21.2.7
    pull_policy: missing
    restart: always
    hostname: daw_db-node
    command:
      [
        "start-single-node",
        "--certs-dir=/certs",
        "--cache=.35",
        "--max-sql-memory=.35"
      ]
    environment:
      - TZ=Asia/Hong_Kong
    tty: false
    expose:
      - 26257
      - 8080
    healthcheck:
      test:
        [
          "CMD",
          "/usr/bin/curl",
          "-f",
          "--insecure",
          "https://localhost:8080/health?ready=1"
        ]
      interval: 5s
      timeout: 5s
      retries: 1
      start_period: 0s
    volumes:
      - type: volume
        source: db-data
        target: /cockroach/cockroach-data
        read_only: false
      - type: bind
        source: /usr/local/etc/daw/db/certs
        target: /certs
        read_only: false
        bind:
          create_host_path: false

  # (Transition) CockroachDB Database Initialization
  database-init:
    container_name: daw_database-init
    image: cockroachdb/cockroach:v21.2.7
    pull_policy: missing
    restart: "no"
    depends_on:
      database:
        condition: service_healthy
    command:
      [
        "sql",
        "--certs-dir=/certs",
        "--user=root",
        "--host=daw_db-node:26257",
        "--file=/init.sql"
      ]
    environment:
      - TZ=Asia/Hong_Kong
    tty: false
    volumes:
      - type: bind
        source: ./setup/cockroachdb/init.sql
        target: /init.sql
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/db/certs/client
        target: /certs
        read_only: false
        bind:
          create_host_path: false

  # Error Scanner
  errorscanner:
    container_name: daw_errorscanner
    build: ../DAW_ErrorScanner
    depends_on:
      database-init:
        condition: service_completed_successfully
    restart: always
    hostname: daw_errorscanner
    user: "${UID}:${GID}"
    environment:
      - TZ=Asia/Hong_Kong
      - CONFIG_FILEPATH=/credentials/errorscanner-config.json
    tty: false
    volumes:
      - type: bind
        source: /usr/local/etc/daw/errorscanner/config.json
        target: /credentials/errorscanner-config.json
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/db/certs/client
        target: /certs
        read_only: false
        bind:
          create_host_path: false

  # EMQX MQTT Broker
  mqttbroker:
    container_name: daw_mqttbroker
    image: emqx/emqx:4.4.3
    pull_policy: missing
    depends_on:
      database-init:
        condition: service_completed_successfully
    restart: always
    hostname: daw_mqttbroker
    ulimits:
      nproc: 2097152
      nofile:
        soft: 2097152
        hard: 2097152
    environment:
      - TZ=Asia/Hong_Kong
      - EMQX_NAME=emqx
    tty: false
    expose:
      - 8081
      - 8880
      - 8883
      - 18083
    healthcheck:
      test: [ "CMD", "/opt/emqx/bin/emqx_ctl", "status" ]
      interval: 5s
      timeout: 25s
      retries: 5
      start_period: 0s
    volumes:
      - type: bind
        source: /usr/local/etc/daw/mqtt/emqx.conf
        target: /opt/emqx/etc/emqx.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/acl.conf
        target: /opt/emqx/etc/acl.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/ssl_dist.conf
        target: /opt/emqx/etc/ssl_dist.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/plugins/emqx_retainer.conf
        target: /opt/emqx/etc/plugins/emqx_retainer.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/plugins/emqx_dashboard.conf
        target: /opt/emqx/etc/plugins/emqx_dashboard.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/plugins/emqx_management.conf
        target: /opt/emqx/etc/plugins/emqx_management.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/plugins/emqx_auth_pgsql.conf
        target: /opt/emqx/etc/plugins/emqx_auth_pgsql.conf
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/certs
        target: /opt/emqx/etc/certs
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/db/certs/client
        target: /opt/emqx/etc/certs/db
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/data/loaded_modules
        target: /opt/emqx/data/loaded_modules
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/data/loaded_plugins
        target: /opt/emqx/data/loaded_plugins
        read_only: false
        bind:
          create_host_path: false
      - type: volume
        source: mqtt-data
        target: /opt/emqx/data/mnesia
        read_only: false
      - type: volume
        source: mqtt-log
        target: /opt/emqx/log
        read_only: false

  # API Server
  apiserver:
    container_name: daw_apiserver
    build: ../DAW_ApiServer
    depends_on:
      mqttbroker:
        condition: service_healthy
    restart: always
    hostname: daw_apiserver
    user: "${UID}:${GID}"
    environment:
      - TZ=Asia/Hong_Kong
      - CONFIG_FILEPATH=/credentials/apiserver-config.json
    tty: false
    expose:
      - 18443
    healthcheck:
      test:
        [
          "CMD",
          "/usr/bin/wget",
          "--no-verbose",
          "--tries=1",
          "--spider",
          "http://localhost:18443/health"
        ]
      interval: 1s
      timeout: 5s
      retries: 1
      start_period: 0s
    volumes:
      - type: bind
        source: /usr/local/etc/daw/apiserver/config.json
        target: /credentials/apiserver-config.json
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/credentials/firebase-service-account.json
        target: /credentials/firebase-service-account.json
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/mqtt/certs/client
        target: /certs/mqtt
        read_only: false
        bind:
          create_host_path: false
      - type: bind
        source: /usr/local/etc/daw/db/certs/client
        target: /certs/db
        read_only: false
        bind:
          create_host_path: false

  # Web Portal
  webportal:
    container_name: daw_webportal
    build: ../DAW_WebPortal
    depends_on:
      apiserver:
        condition: service_healthy
    restart: always
    hostname: daw_webportal
    user: "${UID}:${GID}"
    environment:
      - TZ=Asia/Hong_Kong
    tty: false
    expose:
      - 8080

# Volumes
volumes:

  # EMQX MQTT Broker Data
  mqtt-data:
    external: false

  # EMQX MQTT Broker Log
  mqtt-log:
    external: false

  # CockroachDB Database Data
  db-data:
    external: false

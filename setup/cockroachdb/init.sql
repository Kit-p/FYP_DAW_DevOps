-- create the database
create database if not exists daw;
-- create the operator user
create user if not exists operator with login password null;
-- grant connection access to the operator user
grant connect on database daw to operator;
-- create web console user with monitoring but limited SQL capabilities
create user if not exists console with login password 'console' nosqllogin viewactivity;
-- enabling more monitoring options in the web console
grant admin to console;

-- custom data type User_Role
create type if not exists daw.public.user_role as enum ('temporary', 'operator', 'administrator');
grant usage on type daw.public.user_role to operator;

-- custom data type Issue_Type
create type if not exists daw.public.issue_type as enum ('angelbox', 'server', 'unknown');
grant usage on type daw.public.issue_type to operator;

-- custom data type Angelbox_State
create type if not exists daw.public.angelbox_state as enum ('pending', 'enabled', 'disabled');
grant usage on type daw.public.angelbox_state to operator;

-- table to store error code definitions
create table if not exists daw.public.Error_Code (
    id int2 not null primary key,
    description string not null
);
grant select,insert,update,delete on table daw.public.Error_Code to operator;

-- define error codes
insert into daw.public.Error_Code values (100, 'Not Active'), (101, 'Bluetooth Startup Fail'), (102, 'GPS Startup Fail'), (103, 'GPS no signal') on conflict do nothing;

-- table to store command definitions
create table if not exists daw.public.Command (
    id int2 not null primary key,
    description string not null
);
grant select,insert,update,delete on table daw.public.Command to operator;

-- define commands
insert into daw.public.Command values (1, 'Ping the AngelBox(es)'), (2, 'Reboot the AngelBox(es)'), (3, 'Update the software on the AngelBox(es)') on conflict do nothing;

-- table to store AngelBox information
create table if not exists daw.public.AngelBox (
    id UUID not null primary key default gen_random_ulid(),
    mac_address string not null,
    init_x real check (init_x >= -180 AND init_x <= 180), -- null if mobile box
    init_y real check (init_y >= -90 AND init_y <= 90), -- null if mobile box
    init_z real, -- null if mobile box, optional if static box
    location_en string collate en not null default ('' collate en),
    location_zh string collate zh not null default ('' collate zh),
    gps boolean, -- true if mobile box, otherwise false
    state daw.public.angelbox_state not null,
    version string, -- version of driver program
    create_time timestamptz not null default current_timestamp(),
    family f1 (id, mac_address, create_time),
    family f2 (gps, init_x, init_y, init_z, location_en, location_zh, state, version),
    index (state),
    constraint if_not_pending_then_gps check ( (state = 'pending') or (gps is not null) )
);
grant select,insert,update,delete on table daw.public.AngelBox to operator;

-- table to store heartbeat information
create table if not exists daw.public.Heartbeat (
    id UUID not null unique default gen_random_ulid(),
    angelbox_id UUID not null references daw.public.AngelBox(id) on delete cascade on update cascade,
    current_x real check (current_x >= -180 AND current_x <= 180),
    current_y real check (current_y >= -90 AND current_y <= 90),
    current_z real,
    gps_error real,
    gps_time timestamptz,
    error_codes int2[],
    record_time timestamptz not null, -- timestamp from AngelBox
    insert_time timestamptz not null default current_timestamp(),
    primary key (angelbox_id, record_time),
    unique index (angelbox_id, record_time desc, current_x, current_y, gps_error, gps_time) storing (current_z) where current_x is not null and current_y is not null and gps_error is not null and gps_time is not null
);
grant select,insert,update,delete on table daw.public.Heartbeat to operator;

-- table to store web portal user information
create table if not exists daw.public.User (
    id string not null primary key,
    role daw.public.user_role not null,
    name string not null default '',
    email string not null,
    enabled boolean not null default true,
    create_time timestamptz not null default current_timestamp(),
    family f1 (id, email, create_time),
    family f2 (role, name, enabled),
    index (role) storing (name, email)
);
grant select,insert,update,delete on table daw.public.User to operator;

-- table to store web portal user auth token for API server
create table if not exists daw.public.User_Token (
    user_id string not null primary key references daw.public.User(id) on delete cascade on update cascade,
    token string unique,
    expiry_time timestamptz,
    family f1 (user_id),
    family f2 (token, expiry_time),
    index (expiry_time, token)
);
grant select,insert,update,delete on table daw.public.User_Token to operator;

-- table to store scan result information
create table if not exists daw.public.Scan_Result (
    angelbox_id UUID not null,
    mac_address string not null,
    beacon_uuid string not null, -- UUID of beacon scanned
    major int4 not null, -- major value of beacon scanned
    minor int4 not null, -- minor value of beacon scanned
    rssi int2 not null, -- signal strength of beacon scanned
    location_x real,
    location_y real,
    location_z real,
    gps_error real,
    gps_time timestamptz,
    record_time timestamptz not null, -- timestamp from AngelBox
    insert_time timestamptz not null default current_timestamp(),
    primary key (angelbox_id, mac_address, beacon_uuid, major, minor, rssi, record_time),
    index (angelbox_id, record_time desc, location_x, location_y, gps_error, gps_time) storing (location_z) where location_x is not null and location_y is not null and gps_error is not null and gps_time is not null,
    index (record_time desc, angelbox_id)
);
grant select,insert,update,delete on table daw.public.Scan_Result to operator;

-- table to store issue information
create table if not exists daw.public.Issue (
    id UUID not null unique default gen_random_ulid(),
    group_id UUID not null default gen_random_ulid(), -- same group_id if everything but heartbeat_id / create_time different
    type daw.public.issue_type not null,
    heartbeat_id UUID references daw.public.Heartbeat(id) on delete set null on update cascade,
    angelbox_id UUID references daw.public.AngelBox(id) on delete cascade on update cascade,
    error_code int2 not null references daw.public.Error_Code(id) on delete cascade on update cascade,
    detail_en string collate en not null default ('' collate en),
    detail_zh string collate zh not null default ('' collate zh),
    resolved boolean not null default false,
    resolved_time timestamptz,
    resolved_manually boolean not null default false,
    create_time timestamptz not null default current_timestamp(),
    primary key (group_id, type, angelbox_id, error_code, detail_en, detail_zh, create_time),
    family f1 (id, group_id, type, heartbeat_id, angelbox_id, error_code, detail_en, detail_zh, create_time),
    family f2 (resolved, resolved_time, resolved_manually),
    index (resolved, angelbox_id, type, error_code),
    index (group_id, create_time desc)
);
grant select,insert,update,delete on table daw.public.Issue to operator;

-- table to store command log information
create table if not exists daw.public.Command_Log (
    id UUID not null primary key default gen_random_ulid(),
    command int2 not null references daw.public.Command(id) on delete cascade on update cascade,
    angelbox_ids UUID[] not null,
    data string, -- custom data for the command log as JSON string, e.g., update_url
    user_id string references daw.public.User(id) on delete set null on update cascade,
    create_time timestamptz not null default current_timestamp(),
    index(create_time desc)
);
grant select,insert,update,delete on table daw.public.Command_Log to operator;

-- table to store command response information
create table if not exists daw.public.Command_Response (
    command_id UUID not null references daw.public.Command_Log(id) on delete cascade on update cascade,
    angelbox_id UUID not null references daw.public.AngelBox(id) on delete cascade on update cascade,
    is_success boolean not null default false,
    response string, -- custom message for the command response as JSON string, e.g., msg or err
    record_time timestamptz not null default current_timestamp(),
    primary key (command_id, angelbox_id, is_success, record_time),
    index (command_id, record_time desc)
);
grant select,insert,update,delete on table daw.public.Command_Response to operator;

-- table to store error scan result (by Error Scanner) information
create table if not exists daw.public.Error_Scan_Result (
    id UUID not null primary key default gen_random_ulid(),
    success bool not null,
    start_time timestamptz not null,
    end_time timestamptz not null,
    num_not_active_box int,
    num_box_with_issue int,
    num_unresolved_issue int,
    log string[] not null, -- log messages produced by Error Scanner
    record_time timestamptz not null default current_timestamp(),
    index (record_time desc)
);
grant select,insert,update,delete on table daw.public.Error_Scan_Result to operator;

-- table to store Error Scanner settings
create table if not exists daw.public.Error_Scan_Setting (
    key string not null primary key,
    value int not null
);
grant select,insert,update,delete on table daw.public.Error_Scan_Setting to operator;

-- define Error Scanner settings
insert into daw.public.Error_Scan_Setting (key, value) values ('error_scan_interval', 15), ('max_not_active_time', 10), ('max_gps_no_update_time', 10) on conflict do nothing;

-- view to expose AngelBox real-time status information
create or replace view daw.public.AngelBox_Status as (
    with l as (
        select distinct on (angelbox_id) angelbox_id, current_x, current_y, current_z, gps_error, gps_time
        from daw.public.heartbeat
        where current_x is not null and current_y is not null and gps_error is not null and gps_time is not null
        order by angelbox_id, record_time desc
    ), r as (
        select angelbox_id, max(record_time) as record_time
        from daw.public.heartbeat
        group by angelbox_id
    ), i as (
        select angelbox_id, count(distinct group_id) as num_issue
        from daw.public.issue
        where resolved = false
        group by angelbox_id
    )
    select a.id, a.mac_address, a.init_x, a.init_y, a.init_z, a.location_en, a.location_zh, a.gps, a.state, a.version, a.create_time,
        l.current_x, l.current_y, l.current_z, l.gps_error, l.gps_time, r.record_time as last_active,
        coalesce(i.num_issue, 0) as num_issue
    from daw.public.AngelBox a
        left join l on a.id = l.angelbox_id
        left join r on a.id = r.angelbox_id
        left join i on a.id = i.angelbox_id
    where a.state != 'pending'::daw.public.angelbox_state
);
grant select on table daw.public.AngelBox_Status to operator;

-- view to expose MQTT users for MQTT broker authentication
create or replace view daw.public.MQTT_User as (
    select a.id::string as clientid, 'angelbox' as username
    from daw.public.AngelBox a
    union
    select null as clientid, 'backendworker' as username
    union
    select null as clientid, 'admin' as username
);
grant select on table daw.public.MQTT_User to operator;
